<?php
/**
 * Created by PhpStorm.
 * User: Guper
 * Date: 23.06.2019
 * Time: 14:52
 */
?>


<?=CHtml::beginForm(Yii::app()->createUrl('/site/SaveShortLink'), 'POST', ['enctype' => 'multipart/form-data', 'class' => 'form-group', 'style' => 'margin-top: 50px'])?>
    <input class="form-control" type="text" name="shortLink[url_link]">
    <button class="btn shortLink" type="button" style="padding: 8px 18px 7px; width: 260px;">Укоротить</button>
<?=CHtml::endForm() ?>

<span id="resultLink"></span>

<script type="text/javascript">
    $(document).ready(function () {
        $('.shortLink').on('click', function () {


            $.ajax({
                'url': $('form.form-group').prop('action'),
                'method': 'POST',
                'data': $('form.form-group').serialize(),
                'success': function (response) {
                    $('#resultLink').html('Ваш URL: ' + window.location.host + '/link/' + response);
                }
            });
        });
    })
</script>
