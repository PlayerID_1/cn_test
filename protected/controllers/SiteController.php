<?php

class SiteController extends Controller
{
	public function actionIndex()
	{
		$this->render('index', [
		    'shortLink' => new UrlAlias()
        ]);
	}

	public function actionShortLink($url) {

	    $currentLink = Yii::app()->request->hostInfo . Yii::app()->request->url;

        $oUrlAlias = UrlAlias::model()->findByAttributes(['url_shortlink' => $url]);
        if($oUrlAlias) {
            $this->redirect($oUrlAlias->url_link);
        } else {
            throw new CHttpException(404, 'Url not found');
        }
    }

    public function actionSaveShortLink() {
	    if(Yii::app()->request->isAjaxRequest) {
            $data = Yii::app()->request->getPost('shortLink');
            $oUrlAlias = new UrlAlias();
            $oUrlAlias->setAttributes($data);
            if($oUrlAlias->validate()) {
                $oUrlAlias->save(false);
                echo $oUrlAlias->url_shortlink;
            }

        }
    }

}