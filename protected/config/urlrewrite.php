<?php
/**
 * Created by PhpStorm.
 * User: Guper
 * Date: 23.06.2019
 * Time: 14:05
 */


return [
    'link/<url:\w+>' => 'site/shortLink',

    '<controller:\w+>' => '<controller>/index',
    '<controller:\w+>/<id:\d+>' => '<controller>/view',
    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
];